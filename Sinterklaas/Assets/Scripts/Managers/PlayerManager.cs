﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {

    private InputState inputState;
    private Walk walkBehavior;
    private Jump jumpBehavior;
    private Animator animator;
    private CollisionState collisionState;

    void Awake()
    {
        inputState = GetComponent<InputState>();
        walkBehavior = GetComponent<Walk>();
        animator = GetComponent<Animator>();
        collisionState = GetComponent<CollisionState>();
        jumpBehavior = GetComponent<Jump>();
    }
    
    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (collisionState.standing)
        {
            ChangeAnimationState(0);            
        }

        if (inputState.absVelX > 0)
        {
            ChangeAnimationState(1);
        }
        if (inputState.absVelY > 0 && jumpBehavior.isJumping)
        {
            ChangeAnimationState(2);
        }
	}

    void ChangeAnimationState(int value)
    {
        animator.SetInteger("AnimState", value);
    }
}
