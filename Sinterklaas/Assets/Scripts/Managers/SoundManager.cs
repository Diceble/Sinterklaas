﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    public AudioClip[] audioClip;
    private AudioSource backgroundMusic;
    private AudioSource soundCoin;
    // Use this for initialization
    void Start()
    {
        backgroundMusic = GetComponent<AudioSource>();
        backgroundMusic.enabled = true;
        PlayBackgroudMusic(0);
    }

    public void PlayBackgroudMusic(int clip)
    {
        backgroundMusic.GetComponent<AudioSource>().clip = audioClip[clip];
        backgroundMusic.Play();
        backgroundMusic.loop = true;
    }

    public void PlayCoinSound(int clip)
    {
        if (soundCoin != null)
        {
            Destroy(soundCoin);
        }
        soundCoin = gameObject.AddComponent<AudioSource>();
        soundCoin.clip = audioClip[clip];
        soundCoin.Play();        
    }
}
