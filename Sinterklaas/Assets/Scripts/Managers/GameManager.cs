﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public int points;

	// Use this for initialization
	void Start () 
    {
        points = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (points == 5)
        {
            Application.LoadLevel(1);
        }
	}

    public void AddPoints()
    {
        points += 1;
    }
}
