﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

    //private
    private GameObject player;
    private GameObject bottem;
    private GameObject left;
    private GameObject right;
    private float bottemdistance;

    //public
    public float mindistance;
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        bottem = GameObject.FindGameObjectWithTag("Bottom");    
	}
	
	// Update is called once per frame
    void Update()
    {
        bottemdistance = bottem.transform.position.y + player.transform.position.y;
        if(bottemdistance <= mindistance)
        {
            transform.position = new Vector3(player.transform.position.x, 0, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 3, transform.position.z);
        }
      
    }
}
