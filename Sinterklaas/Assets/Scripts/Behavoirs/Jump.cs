﻿using UnityEngine;
using System.Collections;

public class Jump : AbstractBehavior {

    public bool isJumping;

    public float jumpSpeed;
	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        var canJump = inputState.GetButtonValue(inputButtons[0]);
        var holdtime = inputState.GetButtonHoldTime(inputButtons[0]);
        if (collisionState.standing)
        {
            if (canJump && holdtime < .1f)
            {
                OnJump();
                isJumping = true;
            }
            else
            {
                isJumping = false;
            }
        }

	
	}

    protected virtual void OnJump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
    }
}
