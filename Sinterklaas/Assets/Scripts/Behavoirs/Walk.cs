﻿using UnityEngine;
using System.Collections;

public class Walk : AbstractBehavior {  
    //public
    public float speed = 50f;
    public float rayDistance;
    //private
    private RaycastHit2D hit;
    private Vector2[] direction;
    private int directionPointer;    
    private float velX;
    private float velY;
    private float offset;
    private bool walk;
    private bool sideHit;
    
    // Use this for initialization
	void Start () 
    {
        walk = true;
        direction = new Vector2[] { Vector2.right, Vector2.left, Vector2.zero };
	}
	
	// Update is called once per frame
	void Update ()
    {        
        var right = inputState.GetButtonValue(inputButtons[0]);
        var left = inputState.GetButtonValue(inputButtons[1]);
        if (right && walk || left && walk)
        {
            var tmpSpeed = speed;
            velX = tmpSpeed * (float)inputState.direction;
            rb.velocity = new Vector2(velX, rb.velocity.y);            
        }
        else
        {
            velX = 0;
        }
        rb.velocity = new Vector2(velX, rb.velocity.y);
        if (right)
        {
            directionPointer = 0;
            offset = 0.5f;
        }
        if (left)
        {
            directionPointer = 1;
            offset = -0.5f;
        }
        if (!right && !left)       
            directionPointer = 2;

        Debug.DrawRay(rb.position, direction[directionPointer] * rayDistance,Color.green);
        hit = Physics2D.Raycast(new Vector2(rb.position.x + offset, rb.position.y), direction[directionPointer] * rayDistance);       
        if (hit.collider.tag == "platform" && hit.distance <= rayDistance || hit.collider.tag == "Right" && hit.distance <= rayDistance || hit.collider.tag == "Left" && hit.distance <= rayDistance)
        {
            sideHit = true;
        }
        else
        {
            sideHit = false;
        }

	}
    void OnCollisionExit2D(Collision2D coll)
    {
        walk = true;
        //Debug.Log(walk);
    }

    void OnCollisionStay2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Right" && sideHit || coll.gameObject.tag == "Left" && sideHit || coll.gameObject.tag == "platform" && sideHit)        
            walk = false;                   
    }
}
