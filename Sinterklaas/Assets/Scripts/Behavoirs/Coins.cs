﻿using UnityEngine;
using System.Collections;

public class Coins : MonoBehaviour 
{

    public GameManager gameManager;
    public SoundManager soundManager;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        soundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameManager.AddPoints();
            soundManager.PlayCoinSound(1);
            Destroy(gameObject);
        }
    }
}
