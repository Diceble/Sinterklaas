﻿using UnityEngine;
using System.Collections;

public class Playercontroller : MonoBehaviour {

    private Rigidbody2D rb;
    private InputState inputState;
    private float velX;
    private float velY;

    public Buttons[] input;
    public float speed;
    public float jumpspeed;
    
	// Use this for initialization
	void Start () 
    {
        rb = GetComponent<Rigidbody2D>();
        inputState = GetComponent<InputState>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        var right = inputState.GetButtonValue(input[0]);
        var left = inputState.GetButtonValue(input[1]);
        var up = inputState.GetButtonValue(input[2]);
        velX = speed;
        velY = jumpspeed;

        if (right || left)
        {
            velX *= left ? -1: 1;
        }
        else
        {
            velX = 0;
        }

        if (up)
        {
            velY *= 1;
        }

        rb.velocity = new Vector2(velX, rb.velocity.y);

	}
}
