﻿using UnityEngine;
using System.Collections;

public class Node : IHeapItem<Node>
{
    public Vector2 worldPosition;
    public bool walkable;
    public int gCost, hCost;
    public int gridX, gridY;
    public Node parent;
    int heapIndex;


    public Node(bool _walkable, Vector2 _worldpos, int _gridX, int _gridY)
    {
        walkable = _walkable;
        worldPosition = _worldpos;
        gridX = _gridX;
        gridY = _gridY;
    }

    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }

    public int HeapIndex
    {
        get
        {
            return heapIndex;
        }
        set
        {
            heapIndex = value;
        }
    }

    public int CompareTo(Node nodeToCompare)
    {
        int compare = fCost.CompareTo(nodeToCompare.fCost);
        if (compare == 0)
        {
            compare = hCost.CompareTo(nodeToCompare.hCost);
        }
        return -compare;
    }
}
